# Dockrunner

Dockrunner is a nifty little tool that allows you to easily run software from other distributions on yours, but only if you don't value your disk space.

## How to run things

```
USAGE:
    dockrunner [FLAGS] [OPTIONS] <FILE>

FLAGS:
    -x               Does this application need access to your X Server?
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -a, --arg <ARG>...               Pass an argument to the file.
    -b, --bin <BIN>                  Is the file we're running a package? If so, pass the binary it drops.
    -i, --image <IMAGE>              The name of the image we should use to run the file. We'll do our best to figure it
                                     out on our own.
    -n, --include <INCLUDE>...       Other files we should include in the image.
    -N, --name <NAME>                Set the name of the image we should save to
    -c, --command <PRECOMMAND>...    Commands we should run during the build
    -v, --volume <VOLUME>...         Create docker volumes, with either /dir or /dir:/dir.

ARGS:
    <FILE>    The path to the file we should run.
```

1. Place your target file in a directory on its own.
2. Ensure the target's dependencies are correctly recorded.
   1. If it's a package file, make sure its dependencies are correctly recorded.
   2. If they aren't, pass `-c 'apt install ...'` or similar to install them.
   3. If it's a standalone binary or uncommon package format, you'll need to install all of its dependencies with `-c`.
3. If you're trying to run a binary or uncommon package format, use `-i <image>` to pass the image we should use.
4. If you're trying to run a graphical program, pass `-x` to tell us about it.
   1. You can't just start the program with `docker-compose up` if you're using X, since we need to do some stuff with `xhost` to authorize the container.
5. If you want to save your data, pass `-v <path>` or `-v <host-path>:<guest-path>` to connect your host system to the guest.
6. Make sure you set the name of the image with `-N` otherwise it might make a *ton* of images and eat your disk.