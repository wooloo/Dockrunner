// This file builds Dockerfiles for us.

use super::args::State;

pub fn build_dockerfile(state: &State) -> String {
	format!(
		include_str!("./Dockerfile.template"),
		image = state.image,
		bin = state.bin,
		args = state.args.join(" "),
		pre = state.pre.join(" && "),
		file = state.file,
		include = state.includes.join(" "),
		install = match state.is_pkg {
			true =>
				if state.file.ends_with(".deb") {
					format! {"apt install ./{}", state.file}
				} else if state.file.ends_with(".rpm") {
					format! {"dnf install ./{}", state.file}
				} else {
					String::from("echo 'We don\'t know how to install this, please do so in a precommand'")
				},
			false => String::from("echo 'No installation needed'"),
		}
	)
}

pub fn build_compose(state: &State) -> String {
	let mut volumes: Vec<String> = vec![];
	for i in &state.volumes {
		if i.contains(":") {
			volumes.push(i.to_string())
		} else {
			volumes.push(format!("{i}:{i}", i = i))
		}
	}
	if state.x {
		for i in vec![
			"/tmp/.X11-unix:/tmp/.X11-unix",
			"~/.Xauthority:/root/.Xauthority",
		] {
			volumes.push(String::from(i))
		}
	}
	let mut envs: Vec<String> = vec![];
	if state.x {
		// We need to forward lots of things for X to work
		envs.push(format!(
			"DISPLAY: \"{}\"",
			std::env::var("DISPLAY").unwrap()
		))
	}
	// Start processing for the file
	// Process volumes
	for i in 0..volumes.len() {
		volumes[i] = format!("      - \"{}\"", volumes[i]);
	}
	let volume_string = if volumes.len() > 0 {
		format!("{}\n{}", "volumes:", volumes.join("\n"))
	} else {
		String::from("")
	};
	// Process envs
	for i in 0..envs.len() {
		envs[i] = format!("      {}", envs[i]);
	}
	let env_string = if envs.len() > 0 {
		format!("{}\n{}", "environment:", envs.join("\n"))
	} else {
		String::from("")
	};
	let name = state.file.split("/").last().unwrap_or("???");
	format!(
		include_str!("docker-compose.template.yml"),
		name = name,
		volumes = volume_string,
		env = env_string
	)
}
