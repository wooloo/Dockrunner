mod args;
mod builder;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use tokio::process::Command;

#[tokio::main]
async fn main() {
	let state = args::parse();
	println!("Generating files...");
	let dockerfile = builder::build_dockerfile(&state);
	let compose = builder::build_compose(&state);
	println!("Writing files...");
	let paths = vec![Path::new("./docker-compose.yml"), Path::new("./Dockerfile")];
	let contents = vec![compose, dockerfile];
	for i in 0..2 {
		let mut file = File::create(&paths[i]).unwrap();
		println!("Wrote {}", paths[i].display());
		file.write_all(contents[i].as_bytes()).unwrap();
	}
	println!("Done writing files");
	println!("Building...");
	let name = match state.name {
		None => String::from(state.file.split("/").last().unwrap_or("???")),
		Some(n) => n,
	};
	println!("{}", name);
	let build = Command::new("docker")
		.arg("build")
		.arg("-t")
		.arg(name)
		.arg(".")
		.spawn();
	let build_future = build.expect("failed to spawn");
	let build_status = build_future.await.unwrap();
	if !build_status.success() {
		panic!("Build failed");
	}
	println!("Build successful");
	let mut hostname = String::from("");
	if state.x {
		println!("Doing some special preparations for use of the X server.");
		Command::new("docker-compose")
			.arg("up")
			.arg("--no-start")
			.spawn()
			.unwrap()
			.await
			.unwrap();
		let id = String::from_utf8(
			Command::new("docker-compose")
				.arg("ps")
				.arg("-q")
				.output()
				.await
				.unwrap()
				.stdout,
		)
		.unwrap();
		hostname = String::from_utf8(
			Command::new("docker")
				.arg("inspect")
				.arg("--format='{{ .Config.Hostname }}'")
				.arg(id)
				.output()
				.await
				.unwrap()
				.stdout,
		)
		.unwrap();
		Command::new("xhost")
			.arg(format!("+local:{}", hostname))
			.spawn()
			.unwrap()
			.await
			.unwrap();
	}
	// Run the container
	println!("Starting...");
	match Command::new("docker-compose")
		.arg("up")
		.spawn()
		.unwrap()
		.await
	{
		Ok(_) => println!("Process exited successfully"),
		Err(_) => println!("Process failed"),
	}
	if state.x {
		println!("Removing X host allowances");
		Command::new("xhost")
			.arg(format!("-local:{}", hostname))
			.spawn()
			.unwrap()
			.await
			.unwrap();
	}
}
