extern crate clap;
use clap::{App, Arg};

pub struct State {
	pub file: String,
	pub is_pkg: bool,
	pub bin: String,
	pub args: Vec<String>,
	pub includes: Vec<String>,
	pub pre: Vec<String>,
	pub image: String,
	pub volumes: Vec<String>,
	pub x: bool,
	pub name: Option<String>,
}

pub fn parse() -> State {
	let matches = App::new("Dockrunner")
		.version(env!("CARGO_PKG_VERSION"))
		.author(env!("CARGO_PKG_AUTHORS"))
		.about(env!("CARGO_PKG_DESCRIPTION"))
		.arg(
			Arg::with_name("FILE")
			.help("The path to the file we should run.")
			.takes_value(true)
			.required(true)
		)
		.arg(
			Arg::with_name("BIN")
			.help("Is the file we're running a package? If so, pass the binary it drops.")
			.short("b")
			.long("bin")
			.takes_value(true)
		)
		.arg(
			Arg::with_name("ARG")
			.help("Pass an argument to the file.")
			.short("a")
			.long("arg")
			.multiple(true)
			.takes_value(true)
		)
		.arg(
			Arg::with_name("IMAGE")
			.help("The name of the image we should use to run the file. We'll do our best to figure it out on our own.")
			.short("i")
			.long("image")
			.takes_value(true)
		)
		.arg(
			Arg::with_name("INCLUDE")
			.help("Other files we should include in the image.")
			.short("n")
			.long("include")
			.multiple(true)
			.takes_value(true)
		)
		.arg(
			Arg::with_name("PRECOMMAND")
			.help("Commands we should run during the build")
			.short("c")
			.long("command")
			.multiple(true)
			.takes_value(true)
		)
		.arg(
			Arg::with_name("VOLUME")
			.help("Create docker volumes, with either /dir or /dir:/dir.")
			.short("-v")
			.long("--volume")
			.takes_value(true)
			.multiple(true)
		)
		.arg(
			Arg::with_name("X")
			.help("Does this application need access to your X Server?")
			.short("x")
		)
		.arg(
			Arg::with_name("NAME")
			.help("Set the name of the image we should save to")
			.short("N")
			.long("name")
			.takes_value(true)
		)
		.get_matches();
	// Record some easy things
	// What is our target?
	let file = String::from(matches.value_of("FILE").unwrap());
	// Is it a package we should install? If so, what is its binary called?
	let mut is_pkg = matches.is_present("BIN");
	let bin = String::from(
		matches
			.value_of("BIN")
			.unwrap_or(&format!("./{}", file.split("/").last().unwrap())),
	);
	let args = matches.values_of_lossy("ARG").unwrap_or(vec![]);
	let includes = matches.values_of_lossy("INCLUDE").unwrap_or(vec![]);
	let pre = matches
		.values_of_lossy("PRECOMMAND")
		.unwrap_or(vec![String::from("echo 'no precommands'")]);
	let image = String::from(match matches.value_of("IMAGE") {
		Some(i) => i,
		None => {
			if file.ends_with(".deb") {
				is_pkg = true;
				"ubuntu:latest"
			} else if file.ends_with(".rpm") {
				is_pkg = true;
				"fedora:latest"
			} else {
				panic!("Can't figure out which image to use, pass one with -i")
			}
		}
	});
	let volumes = matches.values_of_lossy("VOLUME").unwrap_or(vec![]);
	let x = matches.is_present("X");
	let name = match matches.value_of("NAME") {
		Some(s) => Some(String::from(s)),
		None => None,
	};
	State {
		args,
		includes,
		is_pkg,
		pre,
		bin,
		file,
		image,
		volumes,
		name,
		x,
	}
}
